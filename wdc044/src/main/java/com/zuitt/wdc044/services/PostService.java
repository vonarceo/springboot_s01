package com.zuitt.wdc044.services;


import com.zuitt.wdc044.models.Post;
import org.springframework.http.ResponseEntity;

public interface PostService {
    // Create a post
    void createPost(String stringToken, Post post);

    // Getting all posts
    Iterable<Post> getPosts();


    // Edit a user post
    ResponseEntity updatePost(Long id, String stringToken, Post post);

    // Delete a user post
    ResponseEntity deletePost(Long id, String stringToken);

    // Retrieve a user's all post
    ResponseEntity retrievePost( String stringToken, Post post);
}
